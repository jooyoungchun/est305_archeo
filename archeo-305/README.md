# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* 2019 Spring EST305 Team Archeo (instructor : Micah Gideon Modell)

* Ruby version : 2.6.3 

* Rails version : 5.1.7

* Database construction: Mariadb(Local),Jawsdb(Heroku)

* Online deployment URL : https://archeo-305.herokuapp.com/

* Customized interface : The goal of this program is to suggest an appropriate customer relation management system solution for Hollys Coffee to improve its services. Hollys coffee does not have any own customer survey system that firm can collect customer ideas and reviews. So this application offers a survey collecting system to Hollys coffee
